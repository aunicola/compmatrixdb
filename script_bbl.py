#!/usr/bin/python
#-*- coding: utf-8 -*-
import argparse
import bblINcsv
import choicenodebbl
##Parser :
parser=argparse.ArgumentParser()
parser.add_argument("infile", type=str, help="File in input on .bbl", metavar='INFILE')
parser.add_argument("--annot", type=str, help="Annotation", metavar='Annotation', default="annot/annot.csv")
parser.add_argument("score", type=float, help="Enrichment score minimum (Positif number)")
parser.add_argument("Pv", type=float, help="Pvalue for functional annotation (Positif number between 0 and 1)", metavar='pvalue')
parser.add_argument("--withoutCHEBI", action="store_true", help="Render on png with Oog of Cytoscape")
parser.add_argument("fileref", type=str, help="File of ref for the bbl file on '_tab.csv'.", metavar='FileRef')
parser.add_argument("--PWRN", type=str, help="Powernode of your choice in bbl file.   (write with \'name powernode\' if powernode name is with special characters)", metavar='Powernode_choice', default='')
args=parser.parse_args()
##Recovering folder:
i = len(args.infile) -1
while (args.infile[i]!='/' and i>=0):
    i -= 1
folder = args.infile[:i]
##Choice Node and functional annotation this node:
if args.PWRN!='':
    Node=choicenodebbl.node(args.infile,args.PWRN)
else:
    Node=choicenodebbl.node(args.infile)
bblINcsv.listGOTERM(Node,args.annot,args.score,args.Pv,folder,args.withoutCHEBI,args.fileref)
