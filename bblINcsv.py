#!/usr/bin/python
#-*- coding: utf-8 -*-
import choicenodebbl
import listidDavid
import searchDavid
import enrichINlist
import os
def listGOTERM(Node,annot,score,Pv,folder,chebi,filetab):
	'''Function for automatization of nodes functional annotation in a powergraph.'''
	conclusion=[["NODE:","Number protein for David:","Number GOTERM:","List of GOTERM"]]
	list_node_david=[]
	for nodemax in range(len(Node)):
		comparaison=[["Power node:","Number protein for David:","Number GOTERM:","Protein delete:","List of GOTERM","Commentary:"]]
		bestlist= [1,0,0]
		node_init=1
		for node in range(len(Node[nodemax][2])):
			assert os.path.isfile(filetab)==True, "File ref for listidDavid has not exist!"
			idDavid=listidDavid.listid(Node[nodemax][2][node],filetab)
			###Data of protein delete:
			delete=''
			for i in range(len(idDavid[2])):
				delete+=str(idDavid[2][i])+' '
			list_david=searchDavid.research(idDavid[1],annot,"UNIPROT_ACCESSION",list_node_david)
			list_node_david=list_david[2]
			lengthlist=0
			listofGOTERM=[]
			commentary=''
			if list_david[1]==True :
				for i in range(len(list_david[0])):
					enrichi=enrichINlist.enrichi(list_david[i][0][0],Node[nodemax][1][node],score,Pv)
					lengthlist+=enrichi[0]
					listofGOTERM.append(enrichi[1])
				if lengthlist == 0:
					commentary+="No enrichment obtained for the power node " + Node[nodemax][1][node]
				else:
					commentary+="Enrichment has resulted in a list of " + str(lengthlist) + " GOTERM for the power node : " + Node[nodemax][1][node]
			else:
				commentary+= "David don't provide of result for a list of " + str(len(idDavid[0]))+ " molecule in the powernode : " + Node[nodemax][1][node]
				listofGOTERM=[[]]
			if Node[nodemax][1][node]==Node[nodemax][0]:
				node_init=node+1
			comparaison.append([Node[nodemax][1][node],len(idDavid[0]),lengthlist,delete,listofGOTERM[0],commentary])
			if lengthlist>bestlist[1] or node==0:
				bestlist=[node+1,lengthlist,len(list_david[0])]
		##Conclussion of comparaison
		if chebi==False:
			commentary2="With the molecule CHEBI and the score annotation of " + str(score) + " and a Pvalue " + str(Pv) + " resultat is : \t\t\t\t"
		else:
			commentary2="Without the molecules CHEBI and with the score annotation of " + str(score) + " and a Pvalue " + str(Pv) + " resultat is : \t\t\t\t"
		if bestlist[0]<node_init:
			commentary2+="Chosen power node must be cut before, to level of " + comparaison[bestlist[0]][0] +" to have a maximum annotation. "
		elif bestlist[0]>node_init:
			commentary2+="Chosen power node is too small to have a maximum annotation, for that " + comparaison[node_init][0] + " must be cut to level of " + comparaison[bestlist[0]][0] + " whether " + str(bestlist[0]-node_init) + " level above. "
		elif bestlist[0]==node_init and bestlist[0]!=0 :
			commentary2+="Chosen powernode " + comparaison[node_init][0] + " has obtained the largest annotation list. "
		elif bestlist[0]==node_init and bestlist[0]==0 :
			if comparaison[bestlist[0]][1]==0:
				commentary2+="No result was obtained because no enrichment has be find. "
			commentary2+="Chosen power node is not cut."
		###Writting conclussion on file:
		with open(str(folder) + "/RESULTAT_FINAL_"+ str(Node[nodemax][0]) + ".csv","w",encoding='UTF-8') as ddl:
			ddl.write(commentary2 + "\n\n")
			ddl.write("Chosen power node is : \t" + Node[nodemax][0] + "\t\t\t\n\n")
			for i in range(len(comparaison)):
				writting=""
				for j in range(len(comparaison[i])):
					writting+= str(comparaison[i][j]) + "\t"
				ddl.write(writting[:-1] + "\n")
		ddl.close()
		donnee=comparaison[node_init][:3]
		donnee.extend(comparaison[node_init][4])
		conclusion.append(donnee)
	###Conclusion all Node with writting file for resume:
	with open(str(folder)+ "/Conclusion_Node.csv","w",encoding='UTF-8') as f:
		for i in range(len(conclusion)):
			writting=""
			for j in range(len(conclusion[i])):
				writting+= str(conclusion[i][j]) + "\t"
			f.write(writting[:-1] + "\n")
		f.close()