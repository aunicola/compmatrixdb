#!/usr/bin/python
#-*- coding: utf-8 -*-
import csv
import os
def convertor(infile):
    ''' Function for convert a mitab file in a lp file.'''
    ##Checking input arguments
    assert type(infile)==str, "File input for convertor has not a string"
    assert infile[-4:]=='.csv', "File input for convertor has not in '.csv'"
    assert os.path.isfile(infile)==True, "File in mitab for the convertion has not exist!"
    ##Recovering filename of input:
    asp_filename=infile[:-4] + ".lp"
    ##Recovering data:
    def opendata(name):
        with open(name,"r") as data:
            reader = csv.reader(data, delimiter="\t")
            return list(reader)
    data = opendata(infile)
    ##Writing in new file of format asp with a interaction between two molecules:
    data2 = []
    with open(asp_filename,"w") as f :
        for i in range(1,len(data)-1):
            donne=[]
            if data[i][2]!='' and data[i][2]!='-':
                if data[i][2][-1]==' ':
                    data[i][2] = data[i][2][0:len(data[i][2])-1]
                donne.append([data[i][2],data[i][0],data[i][4]])
                if data[i][3]!='' and data[i][3]!='-':
                    if data[i][3][-1]==' ':
                        data[i][3] = data[i][3][0:len(data[i][3])-1]
                    donne.append([data[i][3],data[i][1],data[i][5]])
                    data2.append(["edge",donne[0][0],donne[1][0],donne[0][1],donne[1][1],donne[0][2],donne[1][2]])
                    ligne = 'edge("'+data[i][2]+'","'+data[i][3]+'").'
                    f.write(ligne+"\n")
        f.close()
    ###Writting in new file with
    if len(data2)!=0:
        tab_filename = infile[:-4] + "_tab.csv"
        with open(tab_filename,"w",encoding='UTF-8') as f:
            for i in range(len(data2)):
                writting=""
                for j in range(len(data2[i])):
                    writting+= str(data2[i][j]) + "\t"
                f.write(writting[:-1] + "\n")
        f.close()
    return asp_filename,tab_filename