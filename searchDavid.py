#!/usr/bin/python
#-*- coding: utf-8 -*-
import csv
import re
import requests
import os
from bs4 import BeautifulSoup
def research(infile,fileannot,typeannot,list_pwrn_david):
    '''Function for research annotation file of David API.'''
    ##Checking input arguments
    assert type(infile)==str, "Type of infile for research is not string."
    assert infile[-4:]=='.csv' or infile[-4:]=='.txt', "File input for research is not '.csv' or '.txt' "
    assert os.path.isfile(infile)==True, "File for searchDavid has not exist!"
    assert type(fileannot)==str, "Type of file in annotation for research is not string."
    assert fileannot[-4:]=='.csv', "Annotation file for research is not in '.csv' "
    assert os.path.isfile(fileannot)==True, "Annotation file for searchDavid has not exist!"
    ##Recovering input filename:
    i = len(infile) -1
    while (infile[i]!='/' and i>=0):
        i -= 1
    namePWRN=infile[i+1:-4]
    folder=infile[:i]
    ##Open data
    def opendata(name):
        with open(name,"r") as data:
            reader = csv.reader(data, delimiter="\t")
            return list(reader)
    ##List id of gene or protein
    datalist = opendata(infile)
    listid=""
    for i in range(len(datalist)):
        listid += str(datalist[i][0]) + ","
    ##Annotation list:
    dataannot = opendata(fileannot)
    listannot=""
    for i in range(len(dataannot)):
        listannot += str(dataannot[i][0]) + ","
    ##Function for API David request:
    def requestdavid(REQUEST,outhtml,output,name_PWRN,listPWRN):
        result=False
        BASE_URL = 'https://david.ncifcrf.gov/api.jsp?'
        INTERNAL_QUERY_URL = 'https://david.ncifcrf.gov/term2term.jsp'
        REG_FORM = re.compile(r'^document\.apiForm\.([^.]+)\.value="([^"]+)')
        # REG_FORM_ACTION = re.compile(r'^document\.apiForm\.action = "([^"]+)')
        if not(name_PWRN in listPWRN):
            ####TEST SESSION####
            try:
                session = requests.Session()
            except:
                print ("Connexion session problem.")
                print('Request on first page ({})'.format(BASE_URL + REQUEST))
            else:
                js_payload = session.get(BASE_URL + REQUEST).text       
                def get_fields(payload:str):
                    for line in payload.splitlines():
                        match = REG_FORM.match(line)
                        if match:
                            yield match.groups()
                payload = dict(get_fields(js_payload))
                    ####TEST REQUEST####
                try:
                    html_data = session.post(INTERNAL_QUERY_URL, payload).text
                except:
                    print('Request on first page ({})'.format(BASE_URL + REQUEST))
                    print("Problem of first session.")
                    print('FIELDS:', payload)
                    print('Request on second page ({} with fields {})'.format(INTERNAL_QUERY_URL, payload))
                else:
                    ##Create file in html format with this html page request:
                    with open(outhtml,"w",encoding='UTF-8') as ddl:
                        ddl.write(html_data)
                    ddl.close()
                    ##Reading this page html:
                    soup = BeautifulSoup(open(outhtml), "html.parser")
                    url=''
                    for d1 in soup.find_all(href=re.compile("data/download/")):
                        url = 'https://david.ncifcrf.gov/' + str(d1.get("href"))
                        ####TEST URL####
                    try:
                        assert url!=''
                    except:
                        #print ("Not result possible for " + name_PWRN + " , URL is empty because David don't find result.")
                        listPWRN.append(name_PWRN)
                    else:
                        result=True
                            ####TEST REQUEST####
                        try:
                            html_data2 = session.post(url).text
                        except:
                            print ("Problem with this session.")
                        else:
                            ##Create file in csv format with the data of download of this page:
                            with open(output,"w",encoding='UTF-8') as ddl:
                                ddl.write(html_data2)
                            ddl.close()
        return result,listPWRN
    ##Create folders:
    if os.path.exists(folder + "/htmlfile")==False:
        os.makedirs(folder + "/htmlfile")
    if os.path.exists(folder + "/list_David")==False:
        os.makedirs(folder + "/list_David")
    filename_list=[]
    ##Request:
    REQUEST = 'type=' + typeannot + '&ids=' + listid + '&tool=term2term&annot=' + listannot
    result_request=[False,list_pwrn_david]
    ##Check the length of the request:
    assert len(REQUEST)<=2048, "Request for David API has superior at 2048 characters" 
    html_name=folder + "/htmlfile/htmlDAVID_" + namePWRN + ".html"
    filename_list.append([folder + "/list_David/List_David_" + namePWRN + ".csv"])
    result_request=requestdavid(REQUEST,html_name,filename_list[0][0],namePWRN,list_pwrn_david)
    return filename_list,result_request[0],result_request[1]