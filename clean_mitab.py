#!/usr/bin/python
#-*- coding: utf-8 -*-

import csv
import os

def clean(infile,taxon,A,B,chebi):
    ''' Function to clean file mitab.'''
    ###Checking input arguments and folder:
    assert infile[-6:]==".mitab", "File for clean_mitab has not mitab file"
    assert os.path.isfile(infile)==True, "File mitab has not exist!"
    assert os.path.isfile("annot/aliases.csv")==True, "File annot/aliases.csv has not exist!"
    assert os.path.isfile("annot/taxon_aliases.csv")==True, "File annot/taxon_aliases.csv has not exist!"
    assert os.path.isfile("annot/decomposables.csv")==True, "File annot/decomposables.csv has not exist!"
    assert type(A)==int, "1st number for clean_mitab has not a integer"
    assert A>0, "This 1st number for clean_mitab is negatif number instead of positif number not null."
    A=A-1
    assert type(B)==int, "2nd number for clean_mitab has not a integer"
    assert B>0, "This 2nd number for clean_mitab is negatif number instead of positif number not null."
    B=B-1
    assert type(chebi)==bool, "Information of chebi for clean_mitab has not a boolean"
    ###Function to open data and file:
    def opendata(name):
        with open(name,"r") as data:
            reader = csv.reader(data, delimiter="\t")
            return list(reader)
    ###Open datafile:
    data=opendata(infile)
    ###Check data of mitab###
    try :
        assert (data[-1][A].isspace()==False)
        assert(len(data[-1][A])>0)
    except:
        print ("Last row is not value of column '" + str(data[0][A]) + "'.")
        try:
            assert(data[-2][A].isspace()==False)
            assert(len(data[-2][A])>0)
        except:
            print ("Before last row is not as length as first row.")
        else:
            print ("Last row is deleted.")
            data=data[:-1]
    ###Open other datafiles:
    data_aliases=opendata("annot/aliases.csv")
    data_taxon=opendata("annot/taxon_aliases.csv")
    data_decomp=opendata("annot/decomposables.csv")    
    ###Checking taxon:
    listtaxon=["human-mouse","mouse-rat","none"]
    for i in range(len(data_taxon)):
        listtaxon.append(data_taxon[i][0])
    try:
        (taxon in listtaxon)==True
    except:
        print ("Taxon choice is not possibl")
    else:
        ###Taxon
        '''Taxon possible : chicken, human, dog, taurus, sheep, pig, mouse, rat, human-mouse, mouse-rat, none, unknow '''
        tax=[]
        if taxon=="human-mouse":
            tax=["human","mouse"]
        elif taxon=="mouse-rat":
            tax=["mouse","rat"]
        elif taxon=="none":
            tax=["none"]
        else:
            tax=[taxon]
    ##Checking file mitab:
    assert data[0][0]=="#Unique identifier for interactor A", "First row, col 1 has not that is '#Unique identifier for interactor A'"
    assert data[0][1]=="Unique identifier for interactor B", "First row, col 2 has not that is 'Unique identifier for interactor B'"
    assert data[0][2]=="Alternative identifier for interactor A", "First row, col 3 has not 'Alternative identifier for interactor A'"
    assert data[0][3]=="Alternative identifier for interactor B", "First row, col 4 has not 'Alternative identifier for interactor B'"
    assert data[0][A]=="Aliases for A" , "First row, col with le number of A has not 'Aliases for A'"
    assert data[0][B]=="Aliases for B" , "First row, col with le number of B has not 'Aliases for B'"
    assert A<=len(data[0]), "Number for Aliases A is superior has nimber column of the first row."
    assert B<=len(data[0]) , "Number for Aliases B is superior has nimber column of the first row."
    assert data[0][9]=="NCBI Taxonomy identifier for interactor A", "First row, col 10 has not 'NCBI Taxonomy identifier for interactor A'"
    assert data[0][10]=="NCBI Taxonomy identifier for interactor B", "First row, col 11 has not 'NCBI Taxonomy identifier for interactor B'"
    ##Cleaning of data:
    datain=[]                                                           #List for integrin data
    for i in range(1,len(data)):
        for j in range(len(data[i])):
            if data[0][j]=="Aliases for A" or data[0][j]=="Aliases for B":
                k=0
                while(k<len(data[i][j]) and data[i][j][k]!="|"):
                    k+=1
                if (k!=len(data[i][j])):
                    data[i][j]=data[i][j][:k]
            if data[i][j][:10]=="uniprotkb:":                           #Removes unnecessary part of name on first position
                data[i][j]=data[i][j][10:]
            elif data[i][j][:7]=='chebi:"' and data[i][j][-1]=='"':
                data[i][j]=data[i][j][7:-1]
            elif data[i][j][:9]=="matrixdb:":
                data[i][j]=data[i][j][9:]
            if data[i][j][-13:]=="(short label)":                       #Removes unnecessary part of name on last position
                data[i][j]=data[i][j][:-13]
            elif data[i][j][-11:]=="(gene name)":
                data[i][j]=data[i][j][:-11]
            for ka in range(len(data_aliases)):                         #Replace name par new name
                if data[i][j]==data_aliases[ka][1] and (data[0][j]=="Aliases for A" or data[0][j]=="Aliases for B"):
                    data[i][j]=data_aliases[ka][0]
            for kt in range(len(data_taxon)):
                if data[i][j]==data_taxon[kt][1] and (data[0][j]=="NCBI Taxonomy identifier for interactor A" or data[0][j]=="NCBI Taxonomy identifier for interactor B"):
                    data[i][j]=data_taxon[kt][0]  
            for kd in range(len(data_decomp)):
                if data[i][j]==data_decomp[kd][0] and (data[0][j]=="Aliases for A" or data[0][j]=="Aliases for B"):
                    data[i][j]=data_decomp[kd][1]
                    datain.append(data[i])
                    data[i][j]=data_decomp[kd][2]
            for j in range(2,4):
                if data[i][j][:4]=="ebi:":
                    data[i][j-2]=data[i][j]
    ###Adding additional integrins:
    for i in range(len(datain)):
        data.append(datain[i])
    ###Create list for write file
    assert len(tax)>0                                                   #Check taxon list
    datawrite=[]
    for i in range(len(data)):
        donne=[data[i][0],data[i][1],data[i][A],data[i][B],data[i][9],data[i][10]]
        if ((data[i][0][:6]!="CHEBI:" and data[i][1][:6]!="CHEBI:") or chebi==False) and data[i]!=[] and not(donne in datawrite):
            if tax==["none"]:
                datawrite.append(donne)
            else:
                for taxo in range(len(tax)):
                    if (data[i][9]==tax[taxo] or data[i][10]==tax[taxo]) and not(donne in datawrite):
                        datawrite.append(donne)
    ##Recovering input filename:
    i = len(infile) -1
    while (infile[i]!='/' and i>=0):
        i -= 1
    ##Create folder:
    if chebi==True:
        folder = infile[i+1:-6]+"_Without_CHEBI"
    else:
        folder = infile[i+1:-6]
    i=1
    while (os.path.exists(folder)):
        if i==1:
            folder+="_1"
        else:
            j=len(folder)-1
            while (j>0 and folder[j]!='_'):
                j-=1
            folder = folder[:j]+"_" + str(i)
        i += 1
    os.makedirs(folder)
    ###Writing in new file of csv:
    filename=folder + "/" + infile[:-6] + ".csv"
    with open(filename,"w",encoding='UTF-8') as ddl:
        for i in range(1,len(datawrite)):
            writting=""
            for j in range(len(datawrite[i])):
                writting+= str(datawrite[i][j]) + "\t"
            ddl.write(writting[:-1] + "\n")
    ddl.close()
    return filename,folder