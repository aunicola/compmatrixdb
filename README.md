# Scriptmatrixdb

This guide is about installing sciptmatrixdb.py package.

## Installation 

To install package:

	pip install powergrasp --no-cache-dir --no-deps

	pip install pyasp -U --no-cache-dir

	pip install pytest bubbletools networkx requests bs4

More documentation about [PowerGrASP](https://github.com/Aluriak/PowerGrASP) or [Bubble-tools](https://github.com/Aluriak/bubble-tools) can be found in their githubs.

## Automatization of data interaction graph compression

### Sciptmatrixdb.py documentation

Documentation obtained in terminal with the command :	`python scriptmatrixdb.py -h`

	usage: scriptmatrixdb.py [-h] [--graph {powergraph,oriented}]
                         [--interac {human,human-mouse,chicken,mouse,mouse-rat,dog,taurus,rat,sheep,pig,none,unknow}]
                         [--render] [--annot Annotation] [--allpwrn]
                         [--withoutCHEBI] INFILE Number_aliases_A Number_aliases_B score pvalue

	positional arguments:
	  INFILE                File in input on .mitab
	  Number_aliases_A      Number of the colum of aliase A (Positif number)
	  Number_aliases_B      Number of the colum of aliase B (Positif number)
	  score                 Enrichment Score minimum (Positif number)
	  pvalue                Pvalue for functional annotation. (Positif number between 0 and 1)

	optional arguments:
	  -h, --help            Show this help message and exit
	  --graph {powergraph,oriented}
		                Choice between powergraph or oriented graph.
	  --interac {human,human-mouse,chicken,mouse,mouse-rat,dog,taurus,rat,sheep,pig,none,unknow}
		                Choice your taxon filter for graph compression.
	  --render              Render on png with Oog of Cytoscape
	  --annot Annotation    File csv for annotation David with GO terms.
	  --allpwrn             To have max concept ()
	  --withoutCHEBI        Choice not to have molecule with identification chebi. Non-protein molecules will not taken into account.
	  --graphonly           Choice not to have powergraph only.


### Example with the tabulated file "matrixdb_CORE27_example.mitab"

	python scriptmatrixdb.py matrixdb_CORE27_example.mitab 5 6 2 0.05

Running the script `scriptmatrixdb.py` with an input file in named mitab format `matrixdb_CORE27_example.mitab`.

Columns `5` and `6` (`Aliases for A` and `Aliases for B`) contain the interaction's molecules aliases.

Minimum enrichment score required for DAVID clusters is `2`.

P-value of the GO terms selected in this enrichment should not exceed `0.05`.


#### Possible option

- Functional annotation in [DAVID](https://david.ncifcrf.gov/home.jsp) is done by selecting annotation categories. All of these categories can be found in the `annot_all.csv` file. By default, the script takes the `annot.csv` file that contains annotation categories for `Gene Ontology`. For an annotation with DAVID's GO terms coming only from the `GOTERM_BP_DIRECT` category. A sample file named `annot_matrixdb.csv` can be found in` annot` folder and can be used with this command :

		python scriptmatrixdb.py matrixdb_CORE27_example.mitab 5 6 2 0.05 --annot=annot/annot_matrixdb.csv
	
- Option to obtain a compressed graph with only human interactions :

		python scriptmatrixdb.py matrixdb_CORE27_example.mitab 5 6 2 0.05 --interac=human

- Option to obtain a compressed graph with only protein molecules :

		python scriptmatrixdb.py matrixdb_CORE27_example.mitab 5 6 2 0.05 --withoutCHEBI

- Option to get only compressed graphic :

		python scriptmatrixdb.py matrixdb_CORE27_example.mitab 5 6 2 0.05 --graphonly


### Rules of rewriting molecules names

To rewrite the graph compression's molecules names from the origin tabulated "mitab"  file.
Rules for this rewrite have been established. To do this, there are 3 files in the `annot` folder :

* `aliases.csv` is a tabulated file contains 2 columns, with first the molecules name after rewriting then the name before rewriting.
* `decomposables.csv` is a tabulated file contains 3 columns. First column contains the name of the dimeric molecule before rewriting and then in the other 2 columns, the 2 monomeric molecules names rewritten.
* `taxon_aliases.csv` is a tabulated file that contains the species common name and its name found in the tabulated mitab file.


## A proteic list's functional annotation

#### Script_bbl.py documentation

Documentation obtained in terminal with the command :	`python script_bbl.py -h`

	usage: script_bbl.py [-h] [--annot Annotation] [--withoutCHEBI] [--PWRN Powernode_choice] INFILE score pvalue FileRef

	positional arguments:
	  INFILE                File in input on .bbl
	  score                 Enrichment score minimum (Positif number)
	  pvalue                Pvalue for functional annotation (Positif number between 0 and 1)
	  FileRef               File of ref for the bbl file on '_tab.csv'.

	optional arguments:
	  -h, --help            	Show this help message and exit
	  --annot Annotation    	Annotation
	  --withoutCHEBI        	Render on png with Oog of Cytoscape
	  --PWRN Powernode_choice	Powernode of your choice in bbl file. (write with 'name powernode' if powernode name is with special characters)



#### Example of annotation obtained with a powernode using script "sciptmatrixdb.py"

Compressed graph `matrixdb_CORE27_example.bbl`, was formed and placed in the folder `matrixdb_CORE27_example` through the command :

	python scriptmatrixdb.py matrixdb_CORE27_example.mitab 5 6 2 0.05 --graphonly
A powernode's annotation such as a powernode `PWRN-"1,3-dimethyl-2-[2-oxopropyl thio]imidazolium chloride"-2-1` of the `matrixdb_CORE27_example.bbl` file can be done with :	

	python script_bbl.py matrixdb_CORE27_example/matrixdb_CORE27_example.bbl 2 0.05 matrixdb_CORE27_example/matrixdb_CORE27_example_tab.csv --annot=annot/annot_matrixdb.csv --PWRN='PWRN-"1,3-dimethyl-2-[2-oxopropyl thio]imidazolium chloride"-2-1'

This script will then be created for each powernode chosen as for example with a powernode named `PN_name`:

* A `PN_name` folder containing for each direct descending powernode (such as : powernode_name) :

	- A tabulated file `List_of_powernode_name.csv` with a molecules list contained in this powernode.
	- A folder `List_of_powernode_name` with the same name as this file and containing : 
		 * A tabulated file `List_powernode_name.txt` containing the each molecule list with its identifier and its taxonomy.
		 * A tabulated file `powernode_name_listinit.txt` containing molecules' identifiers list, before complementing with identifiers that are exclusive to the MatrixDB database.
		 * A folder `powernode_name.txt` containing the molecules' protein identifiers list used then for the functional annotation with DAVID.
		 * A tabulated file `powernode_name_listdelete.txt` Containing the molecules list not taken into account because identifier unknown or not corresponding to the tabulated file provided (example : `matrixdb_CORE27_example_tab.csv`). (This file is created only if there is at least one molecule that is not taken into account.)
		 * A folder `list_David` containing :
			 - A tabulated file `List_David_powernode_name.csv` retrieved from the DAVID annotation, which contains the GO terms for each cluster.
			 - A tabulated file `LIST_GOTERM_powernode_name.csv` containing the GO terms list selected according to the cluster enrichment score and the parameter defined pvalue.
		 * A folder `htmlfile` with the file obtained from the DAVID web page. (Only if the connection is successful.)
* A tabulated file `RESULTAT_FINAL_PN_name.csv`, containing the set of results (powernodes names studied, Uniprot identifiers numbers taken into account in DAVID, GO terms number found, ...) for this powernode studied.


-> Moreover, a tabulated file `Conclusion_Node` is created with the powernodes studied summary, that is to say those who do not have direct successors. (The biggest powernode possible.)
This file contains powernodes studied (= NODE),  proteins identifiers numbers used to DAVID , GO terms number found, and the exact list of these GO terms.

