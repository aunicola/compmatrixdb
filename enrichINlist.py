#!/usr/bin/python
#-*- coding: utf-8 -*-
'''Traitement du fichier data recupérer sur david pour en tirer la liste des genes ou protéines qui on été cité dans les enrichissements supérieurs à la 
    valeur indiqué en entrée et à la valeur de la pvalue inquiquait elle aussi en entrée.
    Un fichier csv est alors crée avec la liste GOTERM trouvé.'''
import csv
import os
def enrichi(inname,namePWRN,annot,Pv):
    ##Checking input arguments
    assert type(inname)== str and inname[-4:]=='.csv', "Name of file input has to a string or a 'csv' file"
    assert os.path.isfile(inname)==True, "File in input for enrichINlist has not exist!"
    assert type(namePWRN)==str, "Name of PRWN is not a string."
    assert type(annot)==float, "Name file of annotation is not float."
    assert annot>=0.0, "Enrichment score input is negative instead of be positive number."
    assert type(Pv)==float, "Pvalue has to a float"
    assert Pv>=0.0 and Pv<=1.0, "Pvalue input is negative number instead of be positive number between 0 and 1." 
    ##Recovering input filename:
    i = len(inname) -1
    while (inname[i]!='/' and i>=0):
        i -= 1
    folder = inname[:i]
    ##Recovering data:
    def opendata(name):
        with open(name,"r") as data:
            reader = csv.reader(data, delimiter="\t")
            return list(reader)
    data = opendata(inname)    
    ##Choose of clusters with enrichment score better than minimum score:
    cluster = []
    for i in range(len(data)):
        if len(data[i])>0:
            donne = []
            if data[i][0][0:18]=="Annotation Cluster" and float(data[i][1][18:])>=annot:        
                j = i
                while (len(data[j])>0):
                    donne.append(data[j])
                    j += 1
                cluster.append(donne)
    ##Choose of Pvalue lower than minimum value:
    datafile = []
    for i in range(len(cluster)):
        if cluster[i][1][4]=="PValue":
            for k in range(2,len(cluster[i])):
                if float(cluster[i][k][4])<Pv and not(cluster[i][k][1] in datafile):         
                    datafile.append(cluster[i][k][1])
    ##Writing in new file of txt:
    filename=folder + "/LIST_GOTERM_"+ namePWRN + ".csv"
    with open(filename,"w",encoding='UTF-8') as f:
        for i in range(len(datafile)):
            f.write(str(datafile[i]) + "\n")
    f.close()
    return len(datafile),datafile