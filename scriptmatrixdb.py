#!/usr/bin/python
#-*- coding: utf-8 -*-
'''With a input file in mitab format. A file in format bbl is create.
First, the mitab file is clarified in file csv with software python : clean_mitab.
Second, the new csv file has converted in asp format (=.lp) with software convertor_csv_asp.
2 column to mitab file is choice for make edge.
Thirty,  a bbl file is create with sofware python : powergraph for the visuation with cytoscape.
Powernode are choice for make a functional annotation.'''
import argparse
import os
import clean_mitab
import convertor_csv_asp
import bblINcsv
import choicenodebbl
##Parser:
parser=argparse.ArgumentParser()
parser.add_argument("infile", type=str, help="File in input on .mitab", metavar='INFILE')
parser.add_argument("A", type=int, help="Number of the colum of aliase A (Positif number)", metavar='Number_aliases_A')
parser.add_argument("B", type=int, help="Number of the colum of aliase B (Positif number)", metavar='Number_aliases_B')
parser.add_argument("--graph",choices=['powergraph', 'oriented'],default='powergraph', type=str, help="Choice between powergraph or oriented graph.")
parser.add_argument("--interac", choices=['human', 'human-mouse','chicken','mouse','mouse-rat','dog','taurus','rat','sheep','pig','none','unknow'],default='none', type=str, help="Choice your taxon filter for graph compression.")
parser.add_argument("--render", action="store_true", help="Render on png with Oog of Cytoscape")
parser.add_argument("--annot", type=str, help="File csv for annotation David with GO terms.", metavar='Annotation', default="annot/annot.csv")
parser.add_argument("--allpwrn", action="store_true", help="To have max concept ()")
parser.add_argument("score", type=float, help="Enrichment Score minimum (Positif number)")
parser.add_argument("Pv", type=float, help="Pvalue for functional annotation. (Positif number between 0 and 1)", metavar='pvalue')
parser.add_argument("--withoutCHEBI", action="store_true", help="Choice not to have molecule with identification chebi. Non-protein molecules will not taken into account.")
parser.add_argument("--graphonly", action="store_true", help="Choice not to have powergraph only.")
args=parser.parse_args()
##Checking input arguments:
assert args.infile[-6:]=='.mitab', "Infile has not in '.mitab'"
assert os.path.isfile(args.infile)==True, "File in input has not exist!"
assert os.path.isfile("annot/aliases.csv")==True, "File annot/aliases.csv has not exist!"
assert os.path.isfile("annot/taxon_aliases.csv")==True, "File annot/taxon_aliases.csv has not exist!"
assert os.path.isfile("annot/decomposables.csv")==True, "File annot/decomposables.csv has not exist!"
assert type(args.A)==int, "1st number for scriptmatrixdb has not a integer"
assert args.A>0, "This 1st number is negatif number instead of positif or null number."
assert type(args.B)==int, "2nd number for scriptmatrixdb has not a integer"
assert args.B>0, "This 2nd number is negatif number instead of positif or null number."
assert args.annot[-4:]=='.csv', "File for annotation input has not in '.csv'"
assert os.path.isfile(args.annot)==True, "File for the annotation has not exist!"
assert args.score>=0.0, "Enrichment score input is negative instead of be positive number."
assert args.Pv>=0.0 and args.Pv<=1.0, "Pvalue input is negative number instead of be positive number between 0 and 1."
##Cleanning mitab file:
fileclean=clean_mitab.clean(args.infile,args.interac,args.A,args.B,args.withoutCHEBI)
print ("=> Homogenisation and filtration of proteic interactions are made.")
##Conversion csv file in format ASP:
assert os.path.isfile(fileclean[0])==True, "File after clean mitab has not exist!"
file_conversion=convertor_csv_asp.convertor(fileclean[0])
assert os.path.isfile(file_conversion[0])==True, "Asp file after conversion has not exist!"
assert os.path.isfile(file_conversion[1])==True, "Tabulation file after conversion has not exist!"
print ("=> Conversion is made.")
##Graph compression:
outbbl=file_conversion[0][:-3]+".bbl"
os.system('python3 -m powergrasp ' + args.graph + ' -o '+ outbbl + ' '+ file_conversion[0])
assert os.path.isfile(outbbl)==True, "File .bbl after powergraph has not exist!"
print ("=> Graph compression is made.\n")
##Formation d'un compte rendu des relations visibles avec ce powergraph
if args.render==True:
	os.system('java -jar Oog_CommandLineTool2012/Oog.jar -inputfiles='+ outbbl +' -img -f=png')
##Annotation Node
if args.graphonly==False:
	if args.allpwrn==True:						#To have maximal concepts
		Node=choicenodebbl.node(outbbl,'all')
	else:										#To have a random concept
		Node=choicenodebbl.node(outbbl)
	for i in range(len(Node)):
		for j in range(len(Node[i][2])):
			assert os.path.isfile(Node[i][2][j])==True, "File " + Node[i][2][j] + " after choicenodebbl has not exist!"
	bblINcsv.listGOTERM(Node,args.annot,args.score,args.Pv,fileclean[1],args.withoutCHEBI,file_conversion[1])