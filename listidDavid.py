#!/usr/bin/python
#-*- coding: utf-8 -*-

import csv
import os

def listid(filePWRN,fileref):
    ##Checking input arguments
    assert filePWRN[-4:]=='.csv', "File of PWRN has not to '.csv'"
    assert fileref[-4:]=='.csv', "File of ref has not to '.csv'"
    assert os.path.isfile(filePWRN)==True, "File with the powernodes for listidDavid has not exist!"
    assert os.path.isfile(fileref)==True, "File with the reference identifaint uniprot for listidDavid has not exist!"
    ##Create folder:
    folder = filePWRN[:-4]
    i=1
    while (os.path.exists(folder)):
        if i==1:
            folder+="_1"
        else:
            folder = folder[:-2]+"_" + str(i)
        i += 1
    os.makedirs(folder)
    ##Recovering data:
    def opendata(name):
        with open(name,"r") as data:
            reader = csv.reader(data, delimiter="\t")
            return list(reader)
    data = opendata(filePWRN)
    dataref = opendata(fileref)
    ##Create file for writting
    dataset = []
    for i in range(len(data)):
        for k in range(len(dataref)):
            donne=[]
            if dataref[k][1]==data[i][0]:
                donne=[dataref[k][1],dataref[k][3],dataref[k][5]]
            elif dataref[k][2]==data[i][0]:
                donne=[dataref[k][2],dataref[k][4],dataref[k][6]]
            if not(donne in dataset) and donne!=[]:
                dataset.append(donne)
    ##Removes duplicates:
    datadelete = []
    for i in range(len(dataset)):
        for j in range(len(dataset)):
            if dataset[i][0]==dataset[j][0] and dataset[i][2]==dataset[j][2] and i!=j:
                if (dataset[i][1][-2]=="-" and dataset[i][1][:-2]==dataset[j][1]) or (dataset[i][1][-3]=="-" and dataset[i][1][:-3]==dataset[j][1]):
                    if not(dataset[i] in datadelete):
                        datadelete.append(dataset[i])
                elif (dataset[j][1][-2]=="-" and dataset[j][1][:-2]==dataset[i][1]) or (dataset[j][1][-3]=="-" and dataset[j][1][:-3]==dataset[i][1]):
                    if not(dataset[j] in datadelete):
                        datadelete.append(dataset[j])
    for k in range(len(datadelete)):
        dataset.remove(datadelete[k])
    ##Search name of PWRN
    i = len(filePWRN) -1
    while (filePWRN[i-4:i]!='PWRN' and i>=0):
        i -= 1
    PWRN = str(filePWRN[i+1:-4])
    ##Create file of list molecule in PWRN
    filename=folder + "/List_" + PWRN + ".txt"
    with open(filename,"w",encoding='UTF-8') as ddl:
        id=0
        for i in range(len(dataset)):
            writting=""
            for j in range(len(dataset[i])):
                writting+= str(dataset[i][j]) + "\t"
            ddl.write(writting[:-1] + "\n")
            id+=1
    ddl.close()
    ##Writting a new file:
    def writtingfile(name,liste):
        with open(name,"w",encoding='UTF-8') as ddl:
            for i in range(len(liste)):
                ddl.write(str(liste[i]) + "\n")
        ddl.close()
    ##Create Folder
    inputname=folder + "/" + PWRN + "_listinit.txt"
    with open(inputname,"w",encoding='UTF-8') as ddl:
        for i in range(len(dataset)):
            ddl.write(str(dataset[i][1]) + "\n")
    ddl.close()
    ##Create list of id molecule with a file of reference:
    assert os.path.isdir('Listing')==True, "Folder 'Listing' has not exist!"
    assert os.path.isfile("Listing/List_id.csv")==True, "File 'List_id.csv' is not exist!"
    dataID = opendata("Listing/List_id.csv")
    datadelete=[]
    datafile=[]
    for i in range(len(dataset)):
        check=False
        for j in range(len(dataID)):
            if dataset[i][1]==dataID[j][0] and (dataset[i][2]==dataID[j][2] or dataset[i][2]=='unknow'):
                dataset[i][1]=dataID[j][1]
        if dataset[i][1][:4]=="ebi:":
            for j in range(len(dataID)):
                if dataset[i][1][4:]==dataID[j][0] and (dataset[i][2]==dataID[j][2] or dataset[i][2]=='unknow'):
                    check=True
                    if not(dataID[j][1] in datafile):
                        datafile.append(dataID[j][1])
        elif dataset[i][1][:6]=="PFRAG_" or dataset[i][1][:6]=="chebi:":
            check=False
        else :
            check=True
            PRO=False
            for k in range(len(dataset[i][1])-5):
                if dataset[i][1][k:k+5]=="-PRO_":
                    if not(dataset[i][1][:k] in datafile):
                        datafile.append(dataset[i][1][:k])
                    PRO=True
            if PRO==False and not(dataset[i][1] in datafile):
                datafile.append(dataset[i][1])
        if check==False:
            datadelete.append(dataset[i][1])
    ##Create file with molecules no include in final file:
    if len(datadelete)>0:                   
        inputname2bis=inputname[:-13]+"_listdelete.txt"
        writtingfile(inputname2bis,datadelete)
    ##Create final file with Uniprot identifiant:
    inputname2=inputname[:-13]+".txt"
    writtingfile(inputname2,datafile)
    return datafile,inputname2,datadelete