#!/usr/bin/python
#-*- coding: utf-8 -*-
from bubbletools import BubbleTree as bt
import csv
import random
import os
def node(infile,check=''):
    '''Function for listing molecule of node in this powergraph.'''
    ##Checking input arguments
    assert infile[-4:]=='.bbl', "File input has not in '.bbl' file"
    assert os.path.isfile(infile)==True, "File in bbl for choicenode has not exist!"
    ##Recovering data of each power nodes:
        ##Open data of input file:
    def opendata(name):
        assert os.path.isfile(name)==True, "File readding is not exist for choicenodebbl has not exist!"
        with open(name,"r") as data:
            reader = csv.reader(data, delimiter="\t")
            return list(reader)
    ##Writting a new file:
    def writtingfile(name,listmcs1):
        with open(name,"w",encoding='UTF-8') as ddl:
            for i in range(len(listmcs1)):
                ddl.write(str(listmcs1[i]) + "\n")   
        ddl.close()
        assert os.path.isfile(name)==True, "File create is not exist for choicenodebbl has not exist!"
    ##Nodes' molecules recovery:     
    tree = bt.from_bubble_file(infile)
    listdata=[]
    list_pnode=[]
    for pnode in tree.powernodes():
        data = tree.powernode_data(pnode)
        listdata.append([pnode,list(data.contained_pnodes), list(data.contained_nodes), list(tree.powernodes_containing(pnode))])
        list_pnode.append(pnode)
    ##Choose one node among all node:
    listall=[]
    if check=='':
        a = random.randint(0, len(list_pnode)-1)
        try:
            print ("Node chose is " + list_pnode[a])
        except:
            print ("a valait : " + str(a) + " alors qu'il devrait etre compris entre 0 et " + str(len(list_pnode)))
            a = random.randint(0, len(list_pnode))
            print ("Node chose is : " + list_pnode[a])
        else:
            listcheck=[a]
    elif check=='all' and 'all' not in list_pnode:
        listcheck=[]
        for b in range(len(listdata)):
            if len(listdata[b][1])>0:
                listcheck.append(list_pnode.index(listdata[b][0]))
    else:
        try:
            a=list_pnode.index(str(check))
            print ("Node chose is " + list_pnode[a])
        except:
            print (check)
            print ("Ce powernode n'existe pas, essayer avec des '\\\"' en argument, cela pourait marcher.")
            print (list_pnode)
            check = input("Essayer de rentrer le powernode convenablement :\n")
            try:
                a=list_pnode.index(str(check))
                print ("Node chose is " + list_pnode[a])
            except:
                print ("Nope")
                assert a>0
            else:
                listcheck=[a]
        else:
            listcheck=[a]
    for a1 in range(len(listcheck)):
        a=listcheck[a1]
        listPWRN=listdata[a][1][:]
        listPWRN.append(listdata[a][0])
        for j in range(len(listdata[a][3])):
            num_pwrn1=list_pnode.index(listdata[a][3][j])
            if len(listdata[num_pwrn1][2])>len(listdata[a][2]):
                listPWRN.append(listdata[num_pwrn1][0])
        listfilename=[]
        ##Recovering input filename:
        i = len(infile) -1
        while (infile[i]!='/' and i>=0):
            i -= 1
        folder = infile[:i]
        ##Create folder for this chose:
        folder+="/"+ str(listdata[a][0])
        i=1
        while (os.path.exists(folder)):
            if i==1:
                folder+="_1"
            else:
                j=len(folder)-1
                while (j>0 and folder[j]!='_'):
                    j-=1
                folder = folder[:j]+"_" + str(i)
            i += 1
        os.makedirs(folder)
        ##Writting file for each node:
        for i in range(len(listPWRN)):
            num_pwrn2=list_pnode.index(listPWRN[i])
            filename=folder + "/List_of_" + str(listdata[num_pwrn2][0]) + ".csv"
            writtingfile(filename,listdata[num_pwrn2][2])
            listfilename.append(filename)
        listall.append([listdata[a][0],listPWRN,listfilename])
    return listall